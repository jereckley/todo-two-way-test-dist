// todotwowaytest: Custom Elements Define Library, ES Module/ES5 Target
import { defineCustomElement } from './todotwowaytest.core.js';
import {
  TodoTwoWayTest
} from './todotwowaytest.components.js';

export function defineCustomElements(window, opts) {
  defineCustomElement(window, [
    TodoTwoWayTest
  ], opts);
}