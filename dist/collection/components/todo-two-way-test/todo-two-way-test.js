export class TodoTwoWayTest {
    updateNumber(event) {
        this.coolNumber = event.target.value;
    }
    componentDidLoad() {
        this.textInput.value = this.coolNumber.toString();
    }
    render() {
        return (h("div", null,
            h("h4", null, "Ugly in component: "),
            h("h5", null, this.coolNumber),
            h("input", { type: "number", ref: (el) => this.textInput = el, onChange: this.updateNumber.bind(this) })));
    }
    static get is() { return "todo-two-way-test"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "coolNumber": {
            "type": Number,
            "attr": "cool-number",
            "mutable": true
        }
    }; }
    static get style() { return "/**style-placeholder:todo-two-way-test:**/"; }
}
