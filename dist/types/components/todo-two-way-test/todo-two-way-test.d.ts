import '../../stencil.core';
export declare class TodoTwoWayTest {
    coolNumber: number;
    textInput: HTMLInputElement;
    updateNumber(event: any): void;
    componentDidLoad(): void;
    render(): JSX.Element;
}
